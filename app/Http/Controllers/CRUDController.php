<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Illuminate\Http\Request;

class CRUDController extends Controller
{
    
    public function index()
    {
        $students = Student::paginate(10);
        return view('crud.index',compact('students'));
    }

    
    public function create()
    {
        return view('crud.create');
    }

    
    public function store(Request $request)
    {
        // Validate the Field
        $this->validate($request,
            [
                'name'=>'required',
                'roll'=>'required',
                'email'=>'required|email|unique:students,email',
                'phone'=>'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
                'address'=>'required',
                'image' => 'required|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ],

            [
                'image.mimes' => 'File Type Error : Image must be of type jpg, jpeg, png',
                // 'email.unique' => 'Email ID already exists...'
            ],

            [
                'name' => 'First Name',
                'roll' => 'Roll No',
                'email' => 'Email Address',
                'phone' => 'Contact Number',
                'address' => 'Address',
                'image' => 'Photo'
            ]
        );

        $student = new Student();
        $student->roll=$request->roll;
        $student->name=$request->name;
        $student->email=$request->email;
        $student->phone=$request->phone;
        $student->address=$request->address;
        $student->image=$request->file('image')->store('/');
        $student->save();
        return redirect()->route('my-crud.index')->with('message','New Student Created Successfull !');

    }

    
    public function show($id)
    {
        $student = Student::find($id);
        return view('crud.read',compact('student'));
    }


    
    public function edit($id)
    {
        $student = Student::find($id);
        return view('crud.edit',compact('student'));
    }

    
    public function update(Request $request, $id)
    {
        // Validate the Field
        $this->validate($request,
            [
                'name'=>'required',
                'roll'=>'required',
                'email'=>'required|email|unique:students,email,{$this->student->id}',
                'phone'=>'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
                'address'=>'required',
                'image' => 'required|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ],

            [
                'image.mimes' => 'File Type Error : Image must be of type jpg, jpeg, png'
            ],
            
            [
                'name' => 'First Name',
                'roll' => 'Roll No',
                'email' => 'Email Address',
                'phone' => 'Contact Number',
                'address' => 'Address',
                'image' => 'Photo'
            ]
        );


        $student = Student::find($id);
        $student->roll=$request->roll;
        $student->name=$request->name;
        $student->email=$request->email;
        $student->phone=$request->phone;
        $student->address=$request->address;
        $student->image=$request->file('image')->store('/');
        $student->save();
        return redirect()->route('my-crud.index')->with('message','Student Updated Successfull !');
    }

    
    public function destroy($id)
    {
        $student = Student::find($id)->delete();
        return back()->with('message','Student Deleted Successfull !');
    }
}
