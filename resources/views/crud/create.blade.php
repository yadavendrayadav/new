
@extends('crud.main')
   
@section('main-content')
   
<div class="container">
	<div>
  		<br><br>
  	</div>
	<div class="row justify-content-center">
    	<div class="col-md-8">
        	<div class="card">
				<div class="card-header">
      				<center><h3>Create New Student</center></h3>
				</div>

      			<div class="card-body">
      			@if ($errors->any())
					<div class="alert alert-danger">
					    <ul>
					        @foreach ($errors->all() as $error)
					            <li>{{ $error }}</li>
					        @endforeach
					    </ul>
					</div>
				@endif
	 	 		<form action="{{ route('my-crud.store') }}" method="post" enctype="multipart/form-data">
	 	 		{{ csrf_field() }}
			  	<div class="row">
			  		<div class="col-md-4">
			  			<div class="form-group">
				    		<label for="roll">Roll Number</label>
				    		<input type="text" class="form-control" placeholder="Roll Number" name="roll" id="email" value="{{ old('roll') }}">
				 		</div>
			  		</div>
			  		<div class="col-md-8">
			  			<div class="form-group">
				    		<label for="roll">Student Name<span class="required">*</span></label>
				    		<input type="text" class="form-control" placeholder="Student Name" name="name" id="name" value="{{ old('name') }}">
				 		</div>
			  		</div>
			  		<div class="col-md-6">
			  			<div class="form-group">
							<label for="email">Email</label>
						    <input type="email" class="form-control" placeholder="Email" name="email" id="email" value="{{ old('email') }}">
						</div>
			  		</div>
			  		<div class="col-md-6">
			  			<div class="form-group">
						    <label for="phone">Phone</label>
						    <input type="text" class="form-control" placeholder="Phone" name="phone" id="phone" value="{{ old('phone') }}">
						</div>
			  		</div>
			    	<div class="col-md-12">
			  			<div class="form-group">
					    	<label for="address">Address</label>
					    	<input type="text" class="form-control" placeholder="Address" name="address" id="address" value="{{ old('address') }}">
					 	</div>
			  		</div>
					<div class="col-md-12">
            			<div class="form-group">
							<label for="image">Image</label>
                			<input type="file" name="image" class="form-control" placeholder="image" value="{{ old('image') }}">
            			</div>
        			</div>
			  	</div>
			  	<button type="submit" class="btn btn-primary">Add</button>
			  	<a href="{{ route('my-crud.index') }}" class="btn btn-danger">Back</a>
				</form> 
			</div>

   	  	</div>
	  
    </div>
	
</div>
    
@endsection