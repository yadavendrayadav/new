 
@extends('crud.main')
   
@section('main-content')
   
<div class="container">
	<div>
  		<br><br>
  	</div>
     @if(Session::has('message'))
     	<p class="alert alert-success">{{ Session::get('message') }}</p>
     @endif
 	<div class="panel panel-primary">
      <div class="panel-heading">
		<a href="{{ route('my-crud.create') }}" data-toggle="modal" data-target="#addModal" class="btn btn-sm btn-success"><i class="fa fa-plus-circle"></i> Add New</a>
      </div>
      <div class="panel-body">
	 	<table class="table table-hover table-bordered table-stripped">
	 		<thead>
	 			<tr>
	 			<th>S.N</th>
	 			<th>Name</th>
	 			<th>Roll No.</th>
	 			<th>Phone</th>
	 			<th>Email</th>
	 			<th>Address</th>
	 			<th>Image</th>
	 			<th style="width:200px;">Action</th>
	 			</tr>
	 		</thead>
	 		<tbody>
	 		    @foreach ($students as $student)
	 			<tr>
	 			<td>{{ $loop->index+1 }}</td>
	 			<td>{{ $student->name }}</td>
	 			<td>{{ $student->roll }}</td>
	 			<td>{{ $student->phone }}</td>
	 			<td>{{ $student->email }}</td>
	 			<td>{{ $student->address }}</td>
				<td><img src="http://localhost/multigraphics/storage/app/{{ $student->image }}" width="100px"></td>
	 			<td>
	 		<form  method="post" action="{{ route('my-crud.destroy',$student->id) }}" class="delete_form">
                	        {{ csrf_field() }}
                		{{ method_field('DELETE') }}
                		<a href="{{ route('my-crud.edit',$student->id) }}" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                		
						<a href="{{ route('my-crud.show',$student->id) }}" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i></a>


                        <button class="btn btn-xs btn-danger" type="submit" onclick="return confirm('Are You Sure? Want to Delete It.');"><i class="fa fa-trash" aria-hidden="true"></i></button>
                	</form>
	 		</td>
	 		</tr>
	 		@endforeach
	 		</tbody>
	 	</table>
	 	
   	  </div>
    </div>
</div>
    
@endsection