@extends('layout')
  
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>
  
                <div class="card-body">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
  
                    <div class="panel-heading">
                        <h6>Students Record: <br><br>
		                <a href="{{ route('my-crud.index') }}" class="btn btn-sm btn-success">All Students</a>
		                <a href="{{ route('my-crud.create') }}" class="btn btn-sm btn-primary">Add New Students</a>
                        </h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection